#!/bin/bash
#
# Run as root to backup system
# Usage: ./backup.sh -p datbase_password
#
PWD=$(pwd)
HOSTNAME=$(hostname)
TMP_DATE=$(date +"%Y%m%d")
TMP_PATH="/tmp/$TMP_DATE"
TMP_FILE="$HOSTNAME-$TMP_DATE"
SAVE_DIR="/home/dpmc/backups"

# getopt
usage() { echo "Usage: $0 [ -p <db_password> ]" 1>&2; exit 1; }

while getopts ":s:p:" o; do
  case "${o}" in
    s)
      s=${OPTARG}
      ((s == 45 || s == 90)) || usage
      ;;
    p)
      p=${OPTARG}
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

if [ -z "${p}" ]; then
    usage
fi

# executed by root
if [[ $USER != "root" ]]; then
  echo "This script must be run as root, enter password."
  su -l -c "bash $PWD/backup.sh -p ${p}"
  exit
fi

# check db
while ! mysql -u root -p${p}  -e ";" ; do
  echo "Can't connect to database, try different password?"
  exit
done

# setup save location
if [ ! -d $SAVE_DIR ]; then
  mkdir -p $SAVE_DIR
fi

# setup tmp location
if [ ! -d $TMP_PATH ]; then
  mkdir -p $TMP_PATH
fi

# move into tmp location
cd $TMP_PATH

echo
echo "Backing up data to $SAVE_DIR"

# backup configs
echo "... configs $(du -h /etc/ | tail -n1 | awk '{print $1}')"
tar czf etc.tar.gz /etc 2>/dev/null
shasum etc.tar.gz > etc.tar.gz.sha

# backup data
echo "... database"
mysqldump --user=root --password=${p} dpmc \
  --ignore-table=dpmc.poll_tarma \
  --ignore-table=dpmc.unit_tarma \
  --ignore-table=dpmc.heartbeats \
  --ignore-table=dpmc.chunk_data \
  --ignore-table=dpmc.node_details \
  --ignore-table=dpmc.poll_cellwatch \
  --ignore-table=dpmc.unit_cellwatch \
  --ignore-table=dpmc.outages_details \
  --ignore-table=dpmc.bcm_panel_history \
  --ignore-table=dpmc.bcm_circuit_history \
> dpmc.sql

tar czf dpmc.sql.tar.gz dpmc.sql 2>/dev/null
shasum dpmc.sql.tar.gz > dpmc.sql.tar.gz.sha

# backup iptables
echo "... iptables"
iptables-save > $TMP_PATH/iptables.backup

# backup crontab
echo "... crontab"
cp -r /var/spool/cron $TMP_PATH

# Backup DHCP
echo "... dhcp"
cp -r /var/lib/dhcp $TMP_PATH

# backup home
echo "... home $(du -h /home/dpmc/ | tail -n1 | awk '{print $1}')"
tar czfp home.tar.gz --exclude='backup*' --exclude='*.rpm' --exclude='*.gz' --exclude='*.tar' --exclude='*.zip' --exclude='*.log' --exclude='*.sql' /home/dpmc 2>/dev/null
shasum home.tar.gz > home.tar.gz.sha

# bundle everything up
echo "... compressing $(du -h $TMP_PATH | tail -n1 | awk '{print $1}')"
tar czf $SAVE_DIR/$TMP_FILE.tar.gz $TMP_PATH 2>/dev/null

# clean up
rm -rf $TMP_PATH
chown -R dpmc:dpmc $SAVE_DIR

echo
echo Finished, $SAVE_DIR/$TMP_FILE.tar.gz
echo