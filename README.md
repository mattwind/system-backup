# System Backup

This script will backup all the important information for a server

* config files in /etc/
* network settings and host file
* crontabs
* iptable settings
* /home folder
* mysql tables (excluding poll data)

## Usage

`./backup.sh -p database_password`
